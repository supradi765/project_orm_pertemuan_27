<?php

use App\Http\Controllers\CustomerController;
use App\Http\Controllers\BarangController;
use App\Http\Controllers\KamarController;
use App\Http\Controllers\PropertiController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::controller(BarangController::class)->name('barang.')->prefix('master-data/barang')->group(function() {Route::get('/'
    ,
    'index')->name('index');
    Route::get('list'
    ,
    'listTabel')->name('list');

    Route::get('dalamfolder'
    ,
    'getDalamFolder')->name('fileDalamFolder');
    });


Route::controller(CustomerController::class)->name('customer.')->prefix('customer')->group(function()
    {Route::get('/','list')->name('list');
    Route::get('/Input', 'formInput')->name('form-input');
    Route::post('/simpandata', 'simpanData')->name('simpan-data');
    });

 Route::controller(KamarController::class)->name('kamar.')->prefix('list')->group(function()
         {Route::get('/','listKamar')->name('list');
          Route::get('/form-input','formInput')->name('form-input');
          Route::POST('/simpan-data','simpanData')->name('simpan-data');
          Route::get('/form-edit/{id}', 'formEdit')->name('form-edit');
          Route::Post('/edit/{id}', 'editData')->name('edit-data');
          Route::get('/delete-data/{id}', 'deleteData')->name('delete-data');
  });

  Route::controller(PropertiController::class)->name('properti.')->prefix('master/properti')->group(function()
  {Route::get('/','listproperti')->name('list');
   Route::get('/form','formInput')->name('form-input');
   Route::post('/simpandata', 'simpanData')->name('simpan-data');
   Route::get('/delete-data/{id}', 'deleteData')->name('delete-data');
});
